<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="3.0" expand-text="yes"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:lq="http://www.barefootliam.org/"
  xmlns:map="http://www.w3.org/2005/xpath-functions/map"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="xsl lq map xs"
  >

  <!--* Produce a list of included files in an XSLT stylesheet.
      *
      * These can be from xsl:import or xsl:include
      * (one could also use entities or even Apache server-side includes,
      * but this stylesheet does not test for those).
      *
      * Input is a stylesheet;
      * the parameter inputfile must be supplied.
      * Output is a text file.
      *
      * TODO improve fn:transform() handling;
      * TODO add import-query-module and use-package
      *
      * Liam Quin, XSLT Slave, Delightful Computing, 2020
      *
      *-->
  <xsl:param  name="filename" as="xs:string" />

  <!--* drop anything not matched: *-->
  <xsl:mode on-no-match="shallow-skip" />
  <xsl:output method="text" />

  <xsl:template match="/">
    <xsl:call-template name="onefile">
      <xsl:with-param name="inputfile" select="$filename" tunnel="yes"  />
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="onefile">
    <xsl:param name="inputfile" as="xs:string" tunnel="yes" />
    <xsl:param name="indent" as="xs:string" tunnel="yes" select=" '' " />
    <xsl:param name="prefix-length" as="xs:integer" tunnel="yes" select=" -1 " />

    <xsl:if test="$inputfile eq ''">
      <xsl:message terminate="yes">onefile called with empty file</xsl:message>
    </xsl:if>

    <xsl:text>{$indent}{$inputfile}&#10;</xsl:text>

    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="xsl:import|xsl:include">
    <xsl:param name="indent" as="xs:string" tunnel="yes" select=" '' " />
    <xsl:param name="inputfile" as="xs:string" tunnel="yes" />

    <xsl:variable name="remotefile" as="xs:string"
      select="resolve-uri(@href, base-uri(.))"/>
    <xsl:variable name="href" select="@href || ''" as="xs:string" />

    <xsl:if test="$remotefile eq ''">
      <xsl:message terminate="yes">{name()} without an href attribute?</xsl:message>
    </xsl:if>
    <xsl:for-each select="doc($remotefile)">
      <xsl:call-template name="onefile">
	<xsl:with-param name="indent" tunnel="yes" select="$indent || '. . . '" />
	<xsl:with-param name="inputfile" tunnel="yes" select="$href"  />
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="(text()|@*)[contains(., 'transform')]">
    <xsl:param name="indent" tunnel="yes" select=" '  -&gt;  ' " />
    <xsl:if test="contains(., 'stylesheet-location')">
      <xsl:variable name="pattern" as="xs:string">.*['&quot;]stylesheet-location['&quot;]\s*:\s*['&quot;]([^'&quot;]*)['&quot;].*</xsl:variable>

      <xsl:variable name="location" as="xs:string"
        select="replace(., $pattern, '$1')" />
      <xsl:choose>
	<xsl:when test="$location = ''">
	  <!--* matched an empty string, ignore *-->
	</xsl:when>
        <xsl:when test="matches($location, '[()]')">
	  <!--* didn't match - e.g. uses source-node *-->
	</xsl:when>
	<xsl:otherwise>
	  <xsl:variable name="remotefile" as="xs:string"
	    select="resolve-uri($location, base-uri(.))"/>
	  <xsl:for-each select="doc($remotefile)">
	    <xsl:call-template name="onefile">
	      <xsl:with-param name="indent" tunnel="yes" select="$indent || '  -&gt;  '" />
	      <xsl:with-param name="inputfile" tunnel="yes" select="$location || '' " />
	    </xsl:call-template>
	  </xsl:for-each>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
