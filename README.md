# xsltimports

XSLT stylesheet to show what an XSLT stylesheet imports, includes, or calls with fn:transform()

Call it with the XSLT file as input, and the parameter _filename_ set to the filename you want reported.

The stylesheet handles xsl:import, xsl:include, and calls to transform() where the stylesheet location is an explicit filename. It does _not_ handle import-query-module, use-package, more than one call to transform() in the same attribute or text node, and filenames that are not literals.

The output uses three dots to show an import or include, and an -> to show a call t fn:transform().

